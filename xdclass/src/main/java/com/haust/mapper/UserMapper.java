package com.haust.mapper;

import com.haust.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @Auther: csp1999
 * @Date: 2020/08/28/14:31
 * @Description: User Mapper
 */
@Repository
public interface UserMapper {

    // 保存微信登录用户基本信息
    Integer saveUser(@Param("user") User user);

    // 根据openid 查询
    User findByUserOpenid(String openid);

    // 根据主键id 查询
    User findByUserId(Integer id);

    // 更新微信用户基本信息
    void updateUser(@Param("user") User user);
}
