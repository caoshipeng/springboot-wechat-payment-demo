package com.haust.mapper;

import com.haust.entity.VideoOrder;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Auther: csp1999
 * @Date: 2020/08/29/9:14
 * @Description: 订单 Mapper
 */
@Repository
public interface VideoOrderMapper {

    // 新增订单
    int insertVideoOrder(@Param("videoOrder") VideoOrder videoOrder);

    // 根据id 查找订单信息
    VideoOrder findVideoOrderById(int id);

    // 根据 订单唯一标识查找
    VideoOrder findVideoOrderByOutTradeNo(String  outTradeNo);

    // 根据id 删除
    int deleteVideoOrderByIdAndUserId(@Param("id") int id, @Param("userId") int userId);

    // 根据userid 查找用户全部订单
    List<VideoOrder> findUserVideoOrderList(int userId);

    // 根据订单唯一标识更新
    int updateVideoOrderByOutTradeNo(@Param("videoOrder") VideoOrder videoOrder);
}
