package com.haust.mapper;

import com.haust.entity.Video;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Auther: csp1999
 * @Date: 2020/08/26/11:47
 * @Description: 视频Mapper
 */
@Repository
public interface VideoMapper {

    // 查询所有
    List<Video> findAll();

    // 查询
    Video findVideoById(Integer id);

    // 修改
    void updateVideo(@Param("video") Video video);

    // 删除
    void deleteVideoById(Integer id);

    // 新增
    Integer addVideo(@Param("video") Video video);
}
