package com.haust.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(chain = true)
/*
 * @Auther: csp1999
 * @Date: 2020/08/25/22:23
 * @Description: 视频表对应实体类
 */
public class Video implements Serializable {

  private Integer id;// 主键
  private String title;// 视频标题
  private String summary;// 视频概述
  private String coverImg;// 视频封面图
  private Integer viewNum;// 观看数量
  private Integer price;// 视频价格: 单位分
  private Timestamp createTime;// 视频创建时间
  private Integer online;// 视频上线状态: 0表示未上线 1表示上线
  private Double point;// 评分: 默认8.7 最高10分

}
