package com.haust.util;

import java.security.MessageDigest;
import java.util.UUID;

/**
 * @Auther: csp1999
 * @Date: 2020/08/29/13:30
 * @Description: 常用工具类封装, md5, uuid等
 */
public class CommonUtils {

    // 生成 uuid， 即用来标识一笔单，也用做 nonce_str
    public static String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "")// 去掉默认自带的 - 分隔符
                .substring(0, 32);// 截取 32 位
    }

    // MD5 加密工具类
    public static String getMD5String(String data) {
        try {
            // 获取MD5 加密实例
            MessageDigest md = MessageDigest.getInstance("MD5");
            // 获得数组对象
            byte[] array = md.digest(data.getBytes("UTF-8"));
            // 拼接加密字符串
            StringBuilder builder = new StringBuilder();
            for (byte item : array) {
                builder.append(Integer.toHexString((item & 0xFF) | 0x100).substring(1, 3));
            }
            return builder.toString().toUpperCase();// 所有字母大写
        } catch (Exception exception) {
            System.out.println("MD5加密算法出现异常...");
        }
        return null;
    }
}
