package com.haust.util;

import com.haust.entity.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

/*
 * @Auther: csp1999
 * @Date: 2020/08/26/20:25
 * @Description: JWT(Json Web Token) 工具类
 */
public class JWTUtils {
    /*
     * JWT格式组成: 头部header、负载payload、签名signature
     * header :主要是描述签名算法
     * payload :主要描述是加密对象的信息，如用户的id等，也可以加些规范里面的东西,
     *          如iss签发者，发行时间，exp(EXPIRE) 过期时间，sub(SUBJECT) 面向的用户.
     * signature :主要是把前面两部分进行加密，防止别人拿到token进行base解密后篡改token
     */


    // 发行者
    public static final String SUBJECT = "xdclass";

    // 过期时间
    public static final long EXPIRE = 1000 * 60 * 60 * 24 * 7;// 过期时间: 7天,单位毫秒

    // 秘钥
    public static final String APPSECRET = "csp1999";// 自定义秘钥,不能泄露

    /*
     * @方法描述: 生成token方法
     * @参数集合: [user]
     * @返回类型: java.lang.String
     * @作者名称: csp1999
     * @日期时间: 2020/8/26 20:26
     */
    public static String createJsonWebToken(User user) {

        // 如果无用户信息或者 信息不合格则返回 null
        if (user == null || user.getName() == null || user.getHeadImg() == null) {
            return null;
        }

        // 生成token
        String token = Jwts
                /** 头部：header 部分 **/
                .builder()
                /** 负载：payload 部分 **/
                .setSubject(SUBJECT)// 发行者
                .claim("openid", user.getOpenid())// 用户id
                .claim("name", user.getName())// 用户名称
                .claim("img", user.getHeadImg())// 用户头像
                .setIssuedAt(new Date())// 发行时间
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRE))// 过期时间
                /** 签名：signature 部分 **/
                .signWith(SignatureAlgorithm.HS256, APPSECRET)
                // 紧凑拼接 token
                .compact();

        return token;
    }

    /*
     * @方法描述: 检验token方法
     * @参数集合: [token]
     * @返回类型: io.jsonwebtoken.Claims
     * @作者名称: csp1999
     * @日期时间: 2020/8/26 20:43
     */
    public static Claims paraseJsonWebToken(String token) {

        try {
            final Claims claims = Jwts
                    .parser()// 获取解析类
                    .setSigningKey(APPSECRET)// 附上秘钥的值
                    .parseClaimsJws(token)// 解析 token
                    .getBody();// 获取 解析成功的内容

            return claims;
        } catch (Exception e) {
            System.out.println("claims 为 null");
        }
        return null;
    }
}
