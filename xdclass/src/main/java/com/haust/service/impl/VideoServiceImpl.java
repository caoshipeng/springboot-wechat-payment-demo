package com.haust.service.impl;

import com.haust.entity.Video;
import com.haust.mapper.VideoMapper;
import com.haust.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther: csp1999
 * @Date: 2020/08/26/15:22
 * @Description: 视频 Service 实现类
 */
@Service
public class VideoServiceImpl implements VideoService {

    @Autowired
    private VideoMapper videoMapper;

    @Override
    public List<Video> findAll() {
        return videoMapper.findAll();
    }

    @Override
    public Video findVideoById(Integer id) {
        return videoMapper.findVideoById(id);
    }

    @Override
    public Video updateVideo(Video video) {
        videoMapper.updateVideo(video);
        Video updatedVideo = videoMapper.findVideoById(video.getId());
        return updatedVideo;
    }

    @Override
    public void deleteVideoById(Integer id) {
        videoMapper.deleteVideoById(id);
    }

    @Override
    public Integer addVideo(Video video) {
        Integer id = videoMapper.addVideo(video);
        id = video.getId();// 获取新增后的主键id
        return id;
    }
}
