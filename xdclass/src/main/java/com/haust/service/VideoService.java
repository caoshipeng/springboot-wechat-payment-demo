package com.haust.service;

import com.haust.entity.Video;

import java.util.List;

/**
 * @Auther: csp1999
 * @Date: 2020/08/26/15:22
 * @Description: 视频 Service
 */
public interface VideoService {

    // 查询所有
    List<Video> findAll();

    // 查询
    Video findVideoById(Integer id);

    // 修改
    Video updateVideo(Video video);

    // 删除
    void deleteVideoById(Integer id);

    // 新增
    Integer addVideo(Video video);
}
