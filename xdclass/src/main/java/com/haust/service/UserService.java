package com.haust.service;

import com.haust.entity.User;

/**
 * @Auther: csp1999
 * @Date: 2020/08/27/19:18
 * @Description: 用户 Service
 */
public interface UserService {

    // 根据 code 保存微信扫码用户的信息
    User saveWeChatUser(String code);
}
