package com.haust.config;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @Auther: csp1999
 * @Date: 2020/08/26/10:27
 * @Description: 微信相关配置类
 */
@Configuration
/**
 * @PropertySource 注解指定配置文件位置：(属性名称规范: 大模块.子模块.属性名)
 */
@PropertySource(value = "classpath:application.properties")// 从类路径下的application.properties 读取配置

@Data // lombok内置set/get 方法
@Accessors(chain = true) // 链式调用
public class WeChatConfig {

    /**
     * 微信开放平台获取二维码url地址
     * 待填充参数：appid=%s    redirect_uri=%s     state=%s
     */
    private final static String OPEN_QRCODE_URL = "https://open.weixin.qq.com/connect/qrconnect?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_login&state=%s#wechat_redirect";

    /**
     * 微信开放平台/公众平台 获取access_token地址
     * 待填充参数：appid=%s    secret=%s     code=%s
     */
    private final static String OPEN_ACCESS_TOKEN_URL="https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code";

    /**
     * 获取用户信息地址
     * 待填充参数：access_token=%s    openid=%s
     */
    private final static String OPEN_USER_INFO_URL ="https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s&lang=zh_CN";

    /**
     * 微信支付统一下单URL
     */
    private final static String UNIFIED_ORDER_URL = "https://api.xdclass.net/pay/unifiedorder";

    /**
     * 商户号id
     */
    @Value("${wxpay.mer_id}")
    private String mchId;

    /**
     * 支付key
     */
    @Value("${wxpay.key}")
    private String key;

    /**
     * 微信支付回调url
     */
    @Value("${wxpay.callback}")
    private String payCallbackUrl;

    /**
     * 微信appid
     */
    @Value("${wxpay.appid}")
    private String appid;

    /**
     * 微信秘钥
     */
    @Value("${wxpay.appsecret}")
    private String appsecret;

    /**
     * 开放平台appid
     */
    @Value("${wxopen.appid}")
    private String openAppid;

    /**
     * 开放平台秘钥
     */
    @Value("${wxopen.appsecret}")
    private String openAppsecret;

    /**
     * 开放平台回调地址
     */
    @Value("${wxopen.redirect_url}")
    private String openRedirectUrl;

    public static String getUnifiedOrderUrl() {
        return UNIFIED_ORDER_URL;
    }

    public static String getOpenUserInfoUrl() {
        return OPEN_USER_INFO_URL;
    }

    public static String getOpenAccessTokenUrl() {
        return OPEN_ACCESS_TOKEN_URL;
    }

    public static String getOpenQrcodeUrl() {
        return OPEN_QRCODE_URL;
    }
}
