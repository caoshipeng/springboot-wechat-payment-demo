package com.haust.config;

import com.github.pagehelper.PageInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/*
 * @Auther: csp1999
 * @Date: 2020/08/26/18:13
 * @Description: mybaits 分页插件 pageHelper
 */
@Configuration
public class MyBatisConfig {

    @Bean(name = "pageHelper")
    public PageInterceptor pageHelper() {
        PageInterceptor pageHelper = new PageInterceptor ();// pageHelper 声明
        Properties properties = new Properties();

        // offsetAsPageNum 设置为 true 时, 会将 rowBounds 的第一个参数 offset 当成 pageNum 当前页码使用
        properties.setProperty("offsetAsPageNum", "true");

        // rowBoundsWithCount 设置为 true 时, 使用 rowBounds 分页会进行 count 总数量查询
        properties.setProperty("rowBoundsWithCount", "true");

        // reasonable 设置为 true 时, 分页参数合理化:
        // 当启用合理化时，如果 pageNum > pageSize, 默认会查询最后一页的数据;
        // 禁用合理化后, 当 pageNum > pageSize 会返回空数据;
        properties.setProperty("reasonable", "true");

        // pageHelper 5.0.0+ 版本需要新加上以下配置:
        // always总是返回PageInfo类型,check检查返回类型是否为PageInfo,none返回Page
        properties.setProperty("returnPageInfo","check");
        // 支持通过Mapper接口参数来传递分页参数
        properties.setProperty("supportMethodsArguments","false");
        // 配置数据库的方言
        properties.setProperty("helperDialect","mysql");

        pageHelper.setProperties(properties);
        return pageHelper;
    }
}
