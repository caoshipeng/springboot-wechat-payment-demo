package com.haust.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.haust.entity.Video;
import com.haust.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: csp1999
 * @Date: 2020/08/26/15:31
 * @Description: 视频 Controller
 */
@RestController
@RequestMapping("/video")
public class VideoController {

    @Autowired
    private VideoService videoService;

    @GetMapping("/page")
    @CrossOrigin // 跨域
    public Object pageVideo(@RequestParam(name = "currentPage",  defaultValue = "1") int currentPage,
                            @RequestParam(name = "pageSize", defaultValue = "4") int pageSize) {
        PageHelper.startPage(currentPage, pageSize);
        List<Video> list = videoService.findAll();
        PageInfo<Video> pageInfo = new PageInfo<>(list);

        Map<String,Object> data = new HashMap<>();
        data.put("total",pageInfo.getTotal());// 总条数
        data.put("totalPage",pageInfo.getPages());// 总页数
        data.put("currentPage",currentPage);// 当前页
        data.put("data",pageInfo.getList());// 数据

        return data;
    }

}
