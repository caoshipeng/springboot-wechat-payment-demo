package com.haust.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Auther: csp1999
 * @Date: 2020/08/28/18:31
 * @Description: 首页 Controller
 */
@Controller
@RequestMapping("/user")
public class IndexController {

    @GetMapping("/index")
    public String test03(Model model,
                         @RequestParam("token") String token,// 获取url 中的token
                         @ModelAttribute("state") String state) {// 测试videoMapper
        if (token==null){
            return "/error/error";
        }
        System.out.println("=============>"+token);
        model.addAttribute("token", token);
        return "test";
    }

    @GetMapping("/login")
    public String login(){
        return "login";
    }
}
