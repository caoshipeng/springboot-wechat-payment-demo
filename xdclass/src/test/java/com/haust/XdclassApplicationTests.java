package com.haust;

import com.haust.entity.User;
import com.haust.entity.Video;
import com.haust.entity.VideoOrder;
import com.haust.mapper.UserMapper;
import com.haust.mapper.VideoOrderMapper;
import com.haust.service.VideoService;
import com.haust.util.JWTUtils;
import io.jsonwebtoken.Claims;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@SpringBootTest
class XdclassApplicationTests {

    @Autowired
    private VideoService videoService;

    @Test
    void test01() {
        Video video = new Video();
        Integer id = videoService.addVideo(new Video().setTitle("新增测试")
                .setCoverImg("www.baidu.com").setCreateTime(new Timestamp(new Date().getTime())).setSummary("测试数据"));
        System.out.println("新增video 返回其对应id:" + id);
    }

    @Test
    void test02() {
        // Video video = videoService.findById(11);
        Video video = new Video();
        Video updatedVideo = videoService.updateVideo(video.setId(15).setSummary("测试修改4"));
        System.out.println(updatedVideo);
    }

    @Test
    void test03() {
        videoService.deleteVideoById(10);
        System.out.println("删除成功！");
    }

    @Test
    void test04() {
        Video video = videoService.findVideoById(1);
        System.out.println(video);
    }

    @Test
    void test05() {
        User user = new User();
        user.setId(999).setHeadImg("www.baidu.com").setName("csp");

        // 加密：
        String token = JWTUtils.createJsonWebToken(user);
        System.out.println(token);
        /*eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ4ZGNsYXNzIiwiaWQ
        iOjk5OSwibmFtZSI6ImNzcCIsImltZyI6Ind3dy5iYWlkdS5
        jb20iLCJpYXQiOjE1OTg0NTEzODYsImV4cCI6MTU5OTA1NjE
        4Nn0.KkfD1JB_vJ75wmMucaNA6u3WwmsqEOM4-RJCH9n075Y*/
    }

    @Test
    void test06() {
        // 解密
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ4ZGNsYXNzIiwia" +
                "WQiOjk5OSwibmFtZSI6ImNzcCIsImltZyI6Ind3dy5iYWlkdS5jb2" +
                "0iLCJpYXQiOjE1OTg0NTEzODYsImV4cCI6MTU5OTA1NjE4Nn0.Kkf" +
                "D1JB_vJ75wmMucaNA6u3WwmsqEOM4-RJCH9n075Y";

        Claims claims = JWTUtils.paraseJsonWebToken(token);
        if (claims!=null){
            String name = (String) claims.get("name");
            String img = (String) claims.get("img");
            int id = (Integer) claims.get("id");

            System.out.println("解密后：id="+id+",name="+name+",img="+img);
            /** 解密后：id=999,name=csp,img=www.baidu.com **/
        }else {
            System.out.println("解密失败 非法 token");
        }
    }

    @Autowired
    private UserMapper userMapper;

    @Test
    void test07() {
        User user = userMapper.findByUserOpenid("1");
        System.out.println(user);
    }

    @Autowired
    private VideoOrderMapper videoOrderMapper;

    @Test
    void test08() {
        List<VideoOrder> list = videoOrderMapper.findUserVideoOrderList(1);
        list.forEach(System.out::println);

        System.out.println("===============================");
        VideoOrder order = videoOrderMapper.findVideoOrderById(1);
        System.out.println(order);

        System.out.println("===============================");
        VideoOrder order2 = videoOrderMapper.findVideoOrderByOutTradeNo("432werew");
        System.out.println(order2);

        System.out.println("===============================");
        videoOrderMapper.updateVideoOrderByOutTradeNo(order2.setOpenid("abcdef123").setState(1).setNotifyTime(new Date()));

        System.out.println("===============================");
    }
}
